"""The root query type which gives access points into the data universe."""
type Query implements Node {
  """
  Exposes the root query type nested one level down. This is helpful for Relay 1
  which can only query top level fields if they are in a particular form.
  """
  query: Query!

  """
  The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`.
  """
  nodeId: ID!

  """Fetches an object given its globally unique `ID`."""
  node(
    """The globally unique `ID`."""
    nodeId: ID!
  ): Node

  """Reads and enables pagination through a set of `Category`."""
  categories(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `Category`."""
    orderBy: [CategoriesOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: CategoryCondition
  ): CategoriesConnection

  """Reads and enables pagination through a set of `Location`."""
  locations(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `Location`."""
    orderBy: [LocationsOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: LocationCondition
  ): LocationsConnection
  category(xyz_id: Int!): Category
  location(xyz_id: Int!): Location

  """Reads a single `Category` using its globally unique `ID`."""
  categoryByNodeId(
    """The globally unique `ID` to be used in selecting a single `Category`."""
    nodeId: ID!
  ): Category

  """Reads a single `Location` using its globally unique `ID`."""
  locationByNodeId(
    """The globally unique `ID` to be used in selecting a single `Location`."""
    nodeId: ID!
  ): Location
}

"""An object with a globally unique `ID`."""
interface Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
}

"""A connection to a list of `Category` values."""
type CategoriesConnection {
  """A list of `Category` objects."""
  nodes: [Category!]!

  """
  A list of edges which contains the `Category` and cursor to aid in pagination.
  """
  edges: [CategoriesEdge!]!

  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """The count of *all* `Category` you could get from the connection."""
  totalCount: Int!
}

type Category implements Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
  xyz_id: Int!
  xyz_name: String!

  """Reads and enables pagination through a set of `Location`."""
  locations(
    """Only read the first `n` values of the set."""
    first: Int

    """Only read the last `n` values of the set."""
    last: Int

    """
    Skip the first `n` values from our `after` cursor, an alternative to cursor
    based pagination. May not be used with `last`.
    """
    offset: Int

    """Read all values in the set before (above) this cursor."""
    before: Cursor

    """Read all values in the set after (below) this cursor."""
    after: Cursor

    """The method to use when ordering `Location`."""
    orderBy: [LocationsOrderBy!] = [PRIMARY_KEY_ASC]

    """
    A condition to be used in determining which values should be returned by the collection.
    """
    condition: LocationCondition
  ): LocationsConnection!
}

"""A connection to a list of `Location` values."""
type LocationsConnection {
  """A list of `Location` objects."""
  nodes: [Location!]!

  """
  A list of edges which contains the `Location` and cursor to aid in pagination.
  """
  edges: [LocationsEdge!]!

  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """The count of *all* `Location` you could get from the connection."""
  totalCount: Int!
}

type Location implements Node {
  """
  A globally unique identifier. Can be used in various places throughout the system to identify this single value.
  """
  nodeId: ID!
  xyz_id: Int!
  xyz_name: String!
  xyz_categoryId: Int

  """Reads a single `Category` that is related to this `Location`."""
  category: Category
}

"""A `Location` edge in the connection."""
type LocationsEdge {
  """A cursor for use in pagination."""
  cursor: Cursor

  """The `Location` at the end of the edge."""
  node: Location!
}

"""A location in a connection that can be used for resuming pagination."""
scalar Cursor

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: Cursor

  """When paginating forwards, the cursor to continue."""
  endCursor: Cursor
}

"""Methods to use when ordering `Location`."""
enum LocationsOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  CATEGORY_ID_ASC
  CATEGORY_ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

"""
A condition to be used against `Location` object types. All fields are tested
for equality and combined with a logical ‘and.’
"""
input LocationCondition {
  """Checks for equality with the object’s `xyz_id` field."""
  xyz_id: Int

  """Checks for equality with the object’s `xyz_categoryId` field."""
  xyz_categoryId: Int
}

"""A `Category` edge in the connection."""
type CategoriesEdge {
  """A cursor for use in pagination."""
  cursor: Cursor

  """The `Category` at the end of the edge."""
  node: Category!
}

"""Methods to use when ordering `Category`."""
enum CategoriesOrderBy {
  NATURAL
  ID_ASC
  ID_DESC
  PRIMARY_KEY_ASC
  PRIMARY_KEY_DESC
}

"""
A condition to be used against `Category` object types. All fields are tested
for equality and combined with a logical ‘and.’
"""
input CategoryCondition {
  """Checks for equality with the object’s `xyz_id` field."""
  xyz_id: Int
}

"""
The root mutation type which contains root level fields which mutate data.
"""
type Mutation {
  """Creates a single `Category`."""
  createCategory(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: CreateCategoryInput!
  ): CreateCategoryPayload

  """Creates a single `Location`."""
  createLocation(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: CreateLocationInput!
  ): CreateLocationPayload

  """Updates a single `Category` using its globally unique id and a patch."""
  updateCategoryByNodeId(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateCategoryByNodeIdInput!
  ): UpdateCategoryPayload

  """Updates a single `Category` using a unique key and a patch."""
  updateCategory(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateCategoryInput!
  ): UpdateCategoryPayload

  """Updates a single `Location` using its globally unique id and a patch."""
  updateLocationByNodeId(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateLocationByNodeIdInput!
  ): UpdateLocationPayload

  """Updates a single `Location` using a unique key and a patch."""
  updateLocation(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: UpdateLocationInput!
  ): UpdateLocationPayload

  """Deletes a single `Category` using its globally unique id."""
  deleteCategoryByNodeId(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: DeleteCategoryByNodeIdInput!
  ): DeleteCategoryPayload

  """Deletes a single `Category` using a unique key."""
  deleteCategory(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: DeleteCategoryInput!
  ): DeleteCategoryPayload

  """Deletes a single `Location` using its globally unique id."""
  deleteLocationByNodeId(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: DeleteLocationByNodeIdInput!
  ): DeleteLocationPayload

  """Deletes a single `Location` using a unique key."""
  deleteLocation(
    """
    The exclusive input argument for this mutation. An object type, make sure to see documentation for this object’s fields.
    """
    input: DeleteLocationInput!
  ): DeleteLocationPayload
}

"""The output of our create `Category` mutation."""
type CreateCategoryPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Category` that was created by this mutation."""
  category: Category

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `Category`. May be used by Relay 1."""
  categoryEdge(
    """The method to use when ordering `Category`."""
    orderBy: [CategoriesOrderBy!] = [PRIMARY_KEY_ASC]
  ): CategoriesEdge
}

"""All input for the create `Category` mutation."""
input CreateCategoryInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """The `Category` to be created by this mutation."""
  category: CategoryInput!
}

"""An input for mutations affecting `Category`"""
input CategoryInput {
  xyz_id: Int
  xyz_name: String!
}

"""The output of our create `Location` mutation."""
type CreateLocationPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Location` that was created by this mutation."""
  location: Location

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """Reads a single `Category` that is related to this `Location`."""
  category: Category

  """An edge for our `Location`. May be used by Relay 1."""
  locationEdge(
    """The method to use when ordering `Location`."""
    orderBy: [LocationsOrderBy!] = [PRIMARY_KEY_ASC]
  ): LocationsEdge
}

"""All input for the create `Location` mutation."""
input CreateLocationInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """The `Location` to be created by this mutation."""
  location: LocationInput!
}

"""An input for mutations affecting `Location`"""
input LocationInput {
  xyz_id: Int
  xyz_name: String!
  xyz_categoryId: Int
}

"""The output of our update `Category` mutation."""
type UpdateCategoryPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Category` that was updated by this mutation."""
  category: Category

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `Category`. May be used by Relay 1."""
  categoryEdge(
    """The method to use when ordering `Category`."""
    orderBy: [CategoriesOrderBy!] = [PRIMARY_KEY_ASC]
  ): CategoriesEdge
}

"""All input for the `updateCategoryByNodeId` mutation."""
input UpdateCategoryByNodeIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `Category` to be updated.
  """
  nodeId: ID!

  """
  An object where the defined keys will be set on the `Category` being updated.
  """
  patch: CategoryPatch!
}

"""
Represents an update to a `Category`. Fields that are set will be updated.
"""
input CategoryPatch {
  xyz_id: Int
  xyz_name: String
}

"""All input for the `updateCategory` mutation."""
input UpdateCategoryInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  An object where the defined keys will be set on the `Category` being updated.
  """
  patch: CategoryPatch!
  xyz_id: Int!
}

"""The output of our update `Location` mutation."""
type UpdateLocationPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Location` that was updated by this mutation."""
  location: Location

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """Reads a single `Category` that is related to this `Location`."""
  category: Category

  """An edge for our `Location`. May be used by Relay 1."""
  locationEdge(
    """The method to use when ordering `Location`."""
    orderBy: [LocationsOrderBy!] = [PRIMARY_KEY_ASC]
  ): LocationsEdge
}

"""All input for the `updateLocationByNodeId` mutation."""
input UpdateLocationByNodeIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `Location` to be updated.
  """
  nodeId: ID!

  """
  An object where the defined keys will be set on the `Location` being updated.
  """
  patch: LocationPatch!
}

"""
Represents an update to a `Location`. Fields that are set will be updated.
"""
input LocationPatch {
  xyz_id: Int
  xyz_name: String
  xyz_categoryId: Int
}

"""All input for the `updateLocation` mutation."""
input UpdateLocationInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  An object where the defined keys will be set on the `Location` being updated.
  """
  patch: LocationPatch!
  xyz_id: Int!
}

"""The output of our delete `Category` mutation."""
type DeleteCategoryPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Category` that was deleted by this mutation."""
  category: Category
  deletedCategoryNodeId: ID

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """An edge for our `Category`. May be used by Relay 1."""
  categoryEdge(
    """The method to use when ordering `Category`."""
    orderBy: [CategoriesOrderBy!] = [PRIMARY_KEY_ASC]
  ): CategoriesEdge
}

"""All input for the `deleteCategoryByNodeId` mutation."""
input DeleteCategoryByNodeIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `Category` to be deleted.
  """
  nodeId: ID!
}

"""All input for the `deleteCategory` mutation."""
input DeleteCategoryInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String
  xyz_id: Int!
}

"""The output of our delete `Location` mutation."""
type DeleteLocationPayload {
  """
  The exact same `clientMutationId` that was provided in the mutation input,
  unchanged and unused. May be used by a client to track mutations.
  """
  clientMutationId: String

  """The `Location` that was deleted by this mutation."""
  location: Location
  deletedLocationNodeId: ID

  """
  Our root query field type. Allows us to run any query from our mutation payload.
  """
  query: Query

  """Reads a single `Category` that is related to this `Location`."""
  category: Category

  """An edge for our `Location`. May be used by Relay 1."""
  locationEdge(
    """The method to use when ordering `Location`."""
    orderBy: [LocationsOrderBy!] = [PRIMARY_KEY_ASC]
  ): LocationsEdge
}

"""All input for the `deleteLocationByNodeId` mutation."""
input DeleteLocationByNodeIdInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String

  """
  The globally unique `ID` which will identify a single `Location` to be deleted.
  """
  nodeId: ID!
}

"""All input for the `deleteLocation` mutation."""
input DeleteLocationInput {
  """
  An arbitrary string value with no semantic meaning. Will be included in the
  payload verbatim. May be used to track mutations by the client.
  """
  clientMutationId: String
  xyz_id: Int!
}
