import "reflect-metadata";
import {createConnection, getRepository} from "typeorm";
import * as express from "express";
import {postgraphile} from "postgraphile/build/postgraphile";
import * as simplifyInflection from "@graphile-contrib/pg-simplify-inflector"
import {
    makeAddInflectorsPlugin,
    makeExtendSchemaPlugin,
    gql
} from "graphile-utils";
import {Category} from "./entity/Category";
import {Location} from "./entity/Location";
import * as cors from "cors";

createConnection().then(async connection => {

    const app = express();

    // modify queries on database level
    // recommended way to modify on database lvl: COMMENT ON TABLE "public"."user" IS E'@name newNameHere';
    // or modify sql variable from below code snippet for all fields
    //// function removeFieldPluginGenerator(objectName, fieldName) {
    ////   const fn = function(builder) {
    ////     builder.hook("GraphQLObjectType:fields", (fields, sql, { Self }) => {
    const renameOnDBLvl = makeExtendSchemaPlugin(build => {
        return {
            typeDefs: gql`
                extend type User {
                    xyz_id: Int! @requires(columns: ["xyz_id"])
                    xyz_age: Int! @requires(columns: ["xyz_age"])
                    xyz_name: Int! @requires(columns: ["xyz_name"])
                }
            `,
            resolvers: {
                User: {
                    xyz_age: async product => {
                        const {age} = product;
                        return age;
                    },
                },
            },
        };
    });

    // modify queries on graphql level
    const renamePlugin = makeAddInflectorsPlugin(inflectors => {
            const {enumName: oldEnumName} = inflectors;

            return {
                // add: foreign fields disable camel case
                // _columnName(attr: any, _options?: { skipRowId?: boolean }) {
                //     return 'xyz_' + this.coerceToGraphQLName(attr.tags.name || attr.name);
                // },
                column(attr: any) {
                    return 'xyz_' + this._columnName(attr);
                },
            };
        }, true
    );

    const postgraphileOptions: any = {
        subscriptions: true,
        enableCors: true,
        watchPg: true,
        dynamicJson: true,
        setofFunctionsContainNulls: false,
        ignoreRBAC: false,
        ignoreIndexes: false,
        showErrorStack: "json",
        extendedErrors: ["hint", "detail", "errcode"],
        appendPlugins: [simplifyInflection, renamePlugin],
        exportGqlSchemaPath: "schema.graphql",
        graphiql: true,
        enhanceGraphiql: true,
        allowExplain(req) {
            return true;
        },
        enableQueryBatching: true,
        legacyRelations: "omit",
        pgSettings(req) {
        },
    };

    const middleware = postgraphile(
        process.env.DATABASE_URL,
        "public",
        postgraphileOptions
    )


    app.use(
        middleware
    );
    app.use(cors())
    // app.use(jwt)

    // app.use(bodyParser.json());
    // register express routes from defined application routes
    // Routes.forEach(route => {
    //     (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
    //         const result = (new (route.controller as any))[route.action](req, res, next);
    //         if (result instanceof Promise) {
    //             result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);
    //
    //         } else if (result !== null && result !== undefined) {
    //             res.json(result);
    //         }
    //     });
    // });


    // start express server
    app.listen(3000);

    // insert new users for test
    const data = await getRepository(Category).find();

    if (data.length === 0) {
        const cat = new Category();
        cat.id = 1;
        cat.name = "photo";

        await connection.manager.save(connection.manager.create(Category, cat));
        await connection.manager.save(connection.manager.create(Location, {
            name: "west side",
            category: cat
        }));
    }

    console.log("Express server has started on port 3000. Open http://localhost:3000/graphql to see results");

}).catch(error => console.log(error));
