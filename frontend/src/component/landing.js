import ApolloClient from "apollo-client";
import gql from 'graphql-tag'
import {HttpLink, InMemoryCache} from "@apollo/client";
import {Component} from "react";


class Landing extends Component {
    client = new ApolloClient({
        link: new HttpLink({
            uri: 'http://localhost:3000/graphql'
        }),
        cache: new InMemoryCache(),
    });

    componentDidMount() {
        this.client
            .query({
                query: gql`
        {
          locations
            {
            nodes {
                xyz_id
                xyz_name
                category {
                    xyz_name
                }
            }
        }
    }
    `
            })
            .then(result => {
                console.log(result);
                this.setState({data: result.data});
            });
    }

    componentWillUnmount() {
    }

    constructor() {
        super();
        this.state = {data: {message: 'No Data!'}};
    }

    render() {
        return (
            <pre>
                {JSON.stringify(this.state.data)}
            </pre>
        );
    }

}

export default Landing;
